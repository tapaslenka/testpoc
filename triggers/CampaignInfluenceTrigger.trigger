trigger CampaignInfluenceTrigger on Opportunity (before update) {
    
    if(Trigger.isBefore){
        if(Trigger.isUpdate){
            Opportunity oObj = Trigger.New[0];
            //if(Trigger.old[0].Big_House_Conversion__c==True 
            //            && Trigger.new[0].Big_House_Conversion__c==false 
            //                        && Trigger.new[0].Subsidiary__c =='VTI NA')
            //{
            system.debug('inside mail meaase ');
            List<Id> whatIds=new List<Id>();
            whatIds.add(Trigger.New[0].id);
            
            List<Id> idsList = getEmailAddresses();
            EmailTemplate e = [select Id,Name,Subject,body from EmailTemplate where name like :'Test%'];
            String userI = UserInfo.getUserId();
            User[] usr = [SELECT Id, email FROM user WHERE id  =: userI];
            
            //Mass Email
            /*Messaging.MassEmailMessage mail = new Messaging.MassEmailMessage();
            mail.saveAsActivity = false;
            mail.setTargetObjectIds(idsList );
            mail.setTemplateId(e.id);
            mail.setWhatIds(whatIds);
            Messaging.sendEmail(new Messaging.MassEmailMessage[] { mail });*/
            
            //Single Email
            /*
            Messaging.SingleEmailMessage notification = new Messaging.SingleEmailMessage();
            notification.setTargetObjectId(usr[0].id);
            notification.setWhatId(Trigger.New[0].id);
            notification.setTemplateId(e.id);
            notification.setSaveAsActivity(False);  
            notification.setToAddresses( new String[] { 'dhiraj.d.kumar@capgemini.com', 'r.a.lenka@capgemini.com' });              
            //notification.setSenderDisplayName('SF Robot');
            if(!Test.isRunningTest()) {
            List<Messaging.SendEmailResult> postStats =
                        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {notification});
            }*/
            String userId = UserInfo.getUserId();
            String whatId = Trigger.New[0].id;
            String templateId = e.Id;
            Messaging.SingleEmailMessage email = Messaging.renderStoredEmailTemplate(templateId, userId, whatId);

            String emailSubject = email.getSubject();
            String emailTextBody = email.getPlainTextBody();
            
            email.setTargetObjectId(userId);
            email.setSubject(emailSubject);
            email.setPlainTextBody(emailTextBody);
            email.saveAsActivity = false;
            
            if(!Test.isRunningTest()) {
              Messaging.SendEmailResult[] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
            }
            //}
        }
    }
    public static List<Id> getEmailAddresses()
    {
        List<String> idList = new List<String>();
        List<Id> mailToIds = new List<Id>();
        String userI = UserInfo.getUserId();
        User[] usr = [SELECT Id, email FROM user WHERE id  =: userI];
        for(User u : usr) {
            mailToIds.add(u.Id);
        }
        return mailToIds;
    }
}