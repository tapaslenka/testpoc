trigger OrderEventTrigger on Order_Event__e (after insert) {
	List<Task> tasks = new List<task>();
    for (Order_Event__e event : Trigger.New) {
        if (event.Has_Shipped__c  == true) {
            // Create task
            Task cs = new Task();
            cs.Priority = 'High';
            cs.Status = 'New';
            cs.Subject ='Follow up on shipped order ' + event.Order_Number__c;
            cs.OwnerId = UserInfo.getUserId();
            tasks.add(cs);
        }
   }
    
    // Insert all tasks corresponding to events received.
    insert tasks;
}