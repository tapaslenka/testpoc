public class AntDeployUtil{

    public static void deployTestClass(){
    List<ApexTestResult> resultList = [SELECT ApexClass.name,ApexTestRunResultId,AsyncApexJobId,Id,
                                        Message,MethodName,StackTrace,Outcome,RunTime,SystemModstamp 
                                        FROM ApexTestResult order by SystemModstamp DESC];
                                        //WHERE SystemModstamp = TODAY order by SystemModstamp DESC];
    
    Map<String,ApexTestResult> resultList1 = new Map<String,ApexTestResult>();  
    for (ApexTestResult atr : resultList) {
        if(!resultList1.containsKey( atr.ApexClass.Name+atr.MethodName )){
            resultList1.put( atr.ApexClass.Name+atr.MethodName,atr);
        }
    }           
    String fileStr = '';
    String htmlBody = '';
	
    system.debug('-----------------------------------------TEST --------------------------------------');
	
    Map<string,ADC_TestClassSDDMapping__mdt> csMap = new Map<string,ADC_TestClassSDDMapping__mdt>();
    ADC_TestClassSDDMapping__mdt[] csList= [SELECT MasterLabel, DeveloperName, SDDModuleName__c,
                                                        SDDSubModuleName__c FROM ADC_TestClassSDDMapping__mdt];
    for(ADC_TestClassSDDMapping__mdt cObj : csList){
        csMap.put(cObj.MasterLabel,cObj);
    }
    ADC_TestClassSDDMapping__mdt cs;
    htmlBody = '<table border="1" style="border-collapse: collapse"><caption>Test Result Summary</caption><tr><th>SDD Module Name</th><th>SDD Sub Module Name</th><th>Test Class Name</th><th>Method Name</th><th>Result</th><th>Time</th></tr>';
    for (ApexTestResult atr : resultList1.values()) {
        cs = csMap.get(atr.ApexClass.Name);
        if(cs != null){
            fileStr += '\n'+'CSR - '+'Module- '+cs.SDDModuleName__c+' Sub-'+cs.SDDSubModuleName__c+' Class- '+atr.ApexClass.Name+' Method- '+atr.MethodName+' - '+atr.Outcome;
            htmlBody += '<tr><td>'+ cs.SDDModuleName__c +'</td><td>'+ cs.SDDSubModuleName__c + '</td><td>' + atr.ApexClass.Name+ '</td><td>' + atr.MethodName + '</td><td>' + atr.Outcome + '</td><td>' +atr.SystemModstamp +'</td></tr>';
        }else{
            fileStr += '\n'+'CSR - '+'Module- '+'N/A'+' Sub-'+'N/A'+' Class- '+atr.ApexClass.Name+' Method- '+atr.MethodName+' - '+atr.Outcome;
            htmlBody += '<tr><td>'+ 'N/A' +'</td><td>'+ 'N/A' + '</td><td>' + atr.ApexClass.Name+ '</td><td>' + atr.MethodName + '</td><td>' + atr.Outcome + '</td><td>' +atr.SystemModstamp +'</td></tr>';
        }
        if (atr.message != null) {
            fileStr += 'Error Message '+atr.Message+' - '+atr.StackTrace;
            htmlBody += '<tr><td>' + 'Fail Reason-'+atr.ApexClass.Name + '</td><td>' + atr.MethodName + '</td><td>'+atr.StackTrace+'</td><td>'+atr.Message+'</td></tr>';
        }
    }
    htmlBody += '</table>';
    String toMail ='r.a.lenka@capgemini.com';
    String ccMail = 'r.a.lenka@capgemini.com';
    
    Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
    string[] to = new string[] {toMail};
    string[] cc = new string[] {ccMail};
    
    email.setToAddresses(to);
    if(ccMail!=null && ccMail != '')
        email.setCcAddresses(cc);
    email.setSubject('Apex Test Result Mail');
    email.setHtmlBody(htmlBody);
    
    List<Folder> lstFolder = [Select Id , name From Folder where name = 'Deployment History'];
    Document doc = new Document();
    doc.FolderId = lstFolder.get(0).id;
    doc.Body = blob.valueof(fileStr);
    doc.Name = 'Deploy Log '+System.now();

    Database.insert(doc);
    
    try{
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });
    }catch(exception e){
        system.debug('----------ERROR MESSAGE -----------'+e);
    }   
    
    }

}