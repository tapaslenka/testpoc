public class NewAndExistingController {

    public Account account { get; private set; }

    public NewAndExistingController(ApexPages.StandardController stdController) {
        Id id = ApexPages.currentPage().getParameters().get('id');
        this.account = (Account)stdController.getRecord();

    }

    public PageReference save() {
        try {
            upsert(account);
        } catch(System.DMLException e) {
            ApexPages.addMessages(e);
            return null;
        }
        //  After successful Save, navigate to the default view page
        PageReference redirectSuccess = new ApexPages.StandardController(Account).view();
        return (redirectSuccess);
    }
}