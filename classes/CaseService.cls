global class CaseService {
    global class CaseCloseInfo {
        global Id caseId;
        global String caseCloseReason;
    }
    global static void closeCases(List<CaseCloseInfo> CaseCloseInfoList) {
        
        Map<Id,String> caseReasonmap = new map<id,String>(); 
        for(CaseCloseInfo csObj : CaseCloseInfoList){
            caseReasonmap.put(csObj.caseId,csObj.caseCloseReason);
        }
        List<Case> caseList = [select id , reason ,Status from case where id in : caseReasonmap.keySet()];
        for(Case c : caseList){
            c.status = 'Closed';
            c.Reason = caseReasonmap.get(c.id);
        }
        database.update(caseList,false);
    }
}