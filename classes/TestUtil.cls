public class TestUtil {
    public static String htmlCode {get;set;}
    //Enqueue all classes ending in "Test". 
    public static ID enqueueTests() {
        ApexClass[] testClasses = 
           [SELECT Id FROM ApexClass 
            WHERE Name LIKE '%Test'];
        List<Id> classIdList = new List<Id>();
        if (testClasses.size() > 0) {
            List<ApexTestQueueItem> queueItems = new List<ApexTestQueueItem>();
            for (ApexClass cls : testClasses) {
                queueItems.add(new ApexTestQueueItem(ApexClassId=cls.Id));
                classIdList.add(cls.id);
            }

            insert queueItems;
            system.debug('Queue Item List '+queueItems);
            // Get the job ID of the first queue item returned.
            ApexTestQueueItem item = 
               [SELECT ParentJobId ,status ,TestRunResultID FROM ApexTestQueueItem 
                WHERE Id in :queueItems LIMIT 1];
            system.debug('Queue Item List '+item); 
            
            List<AsyncApexJob> jobList = [SELECT ApexClassId,ExtendedStatus,Id,JobItemsProcessed,JobType,
                                    LastProcessed,MethodName,NumberOfErrors,ParentJobId 
                                    FROM AsyncApexJob   where ParentJobId =:item.parentjobid]; 
            
            system.debug('Async Item List '+jobList);   
            /*if(!jobList.isEmpty()){
            Id asyncjobId = jobList.get(0).id;
            system.debug('Async Item List '+asyncjobId); 
            ApexTestResult[] results = 
                                   [SELECT Outcome, ApexClass.Name, MethodName, Message, StackTrace 
                                    FROM ApexTestResult 
                                    WHERE AsyncApexJobId=:asyncjobId];
                                    
            system.debug('Apex Test Result List '+results);
            }
            checkMethodStatus(item.parentjobid); 
            checkClassStatus(item.parentjobid); */                    
            return item.parentjobid;
        }
        return null;
    }

    
    public static void checkClassStatus(ID jobId) {
        system.debug('---checkClassStatus---');
        ApexTestQueueItem[] items = 
           [SELECT ApexClass.Name, Status, ExtendedStatus 
            FROM ApexTestQueueItem 
            WHERE ParentJobId=:jobId];
            
        for (ApexTestQueueItem item : items) {
            String extStatus = item.extendedstatus == null ? '' : item.extendedStatus;
            System.debug(item.ApexClass.Name + ': ' + item.Status + extStatus); 
        }
    }

    
    public static void checkMethodStatus(ID jobId) {
        system.debug('---checkMethodStatus---'+jobId);
        
        List<ApexTestRunResult> resultList = [SELECT AsyncApexJobId,ClassesCompleted,ClassesEnqueued,CreatedById,CreatedDate,
                                    MethodsCompleted,MethodsEnqueued,MethodsFailed,StartTime,Status 
                                    FROM ApexTestRunResult order by createdDate DESC];
        Id jobResId =   resultList.get(0).id;                          
        ApexTestResult[] results = 
           [SELECT Outcome, ApexClass.Name, MethodName, Message, StackTrace 
            FROM ApexTestResult 
            WHERE AsyncApexJobId=:jobResId];
            
        for (ApexTestResult atr : results) {
            System.debug(atr.ApexClass.Name + '.' + atr.MethodName + ': ' + atr.Outcome);
            if (atr.message != null) {
                System.debug(atr.Message + '\n at ' + atr.StackTrace);
            }
        }
    }
    
    public static void checkMethodStatus() {
        system.debug('---checkMethodStatus---');
        
        List<ApexTestRunResult> resultList = [SELECT AsyncApexJobId,ClassesCompleted,ClassesEnqueued,CreatedById,CreatedDate,
                                    MethodsCompleted,MethodsEnqueued,MethodsFailed,StartTime,Status 
                                    FROM ApexTestRunResult order by createdDate DESC];
        Id jobResId =   resultList.get(0).AsyncApexJobId;   
        system.debug('---checkMethodStatus---'+jobResId);                       
        ApexTestResult[] results = 
           [SELECT Outcome, ApexClass.Name, MethodName, Message, StackTrace 
            FROM ApexTestResult 
            WHERE AsyncApexJobId=:jobResId];
        system.debug('---checkMethodStatus--results-'+results);   
        for (ApexTestResult atr : results) {
            System.debug(atr.ApexClass.Name + '.' + atr.MethodName + ': ' + atr.Outcome);
            if (atr.message != null) {
                System.debug(atr.Message + '\n at ' + atr.StackTrace);
            }
        }
        String toMail ='tapasrlenka@gmail.com';
        String ccMail = '';
        String htmlBody = '';

        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        string[] to = new string[] {toMail};
        string[] cc = new string[] {ccMail};
        
        email.setToAddresses(to);
        
        if(ccMail!=null && ccMail != '')
            email.setCcAddresses(cc);
        
        
        email.setSubject('Apex Test Result Mail');
        
        htmlBody = '<table border="1" style="border-collapse: collapse"><caption>Test Result Summary</caption><tr><th>Test Class Name</th><th>Method Name</th><th>Result</th></tr>';
        for (ApexTestResult atr : results) {
            //System.debug(atr.ApexClass.Name + '.' + atr.MethodName + ': ' + atr.Outcome);
            htmlBody += '<tr><td>' + atr.ApexClass.Name+ '</td><td>' + atr.MethodName + '</td><td>' + atr.Outcome + '</td></tr>';
            if (atr.message != null) {
                //System.debug(atr.Message + '\n at ' + atr.StackTrace);
                htmlBody += '<tr><td>' + 'Reason' + '</td><td>' + atr.StackTrace + '</td><td>'+atr.Message+'</td></tr>';

            }
        }
        htmlBody += '</table>';
        htmlCode = htmlBody;
        email.setHtmlBody(htmlBody);
        try{
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });
        }catch(exception e){
            apexpages.addmessage(new apexpages.message(apexpages.severity.error,e.getMessage()));
        }
        
    }
    
}