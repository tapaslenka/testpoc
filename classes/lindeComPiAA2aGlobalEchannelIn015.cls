//Generated by wsdl2apex

public class lindeComPiAA2aGlobalEchannelIn015 {
    public class HTTPS_Port {
        public String endpoint_x = 'https://rel.connector.linde.grp:443/XISOAPAdapter/MessageServlet?senderParty=&senderService=NCNA_REU_ECHANNEL&receiverParty=&receiverService=&interface=GetPriceList&interfaceNamespace=urn%3Alinde.com%3Api%3AA_A2A_GLOBAL%3AeChannel%3AIN015';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://www.openapplications.org/oagis/9', 'wwwOpenapplicationsOrgOagis9', 'http://www.ibm.com/xmlns/prod/commerce/9/price', 'wwwIbmComXmlnsProdCommerce9Price', 'urn:linde.com:pi:A_A2A_GLOBAL:eChannel:IN015', 'lindeComPiAA2aGlobalEchannelIn015', 'http://www.ibm.com/xmlns/prod/commerce/9/foundation', 'wwwIbmComXmlnsProdCommerce9Founda'};
        public wwwIbmComXmlnsProdCommerce9Price.ShowPriceListType GetPriceList() {
            wwwIbmComXmlnsProdCommerce9Price.PriceListLang request_x = new wwwIbmComXmlnsProdCommerce9Price.PriceListLang();
            wwwIbmComXmlnsProdCommerce9Price.ShowPriceListType response_x;
            Map<String, wwwIbmComXmlnsProdCommerce9Price.ShowPriceListType> response_map_x = new Map<String, wwwIbmComXmlnsProdCommerce9Price.ShowPriceListType>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'http://sap.com/xi/WebService/soap1.1',
              'http://www.ibm.com/xmlns/prod/commerce/9/price',
              'GetPriceList',
              'http://www.ibm.com/xmlns/prod/commerce/9/price',
              'ShowPriceList',
              'wwwIbmComXmlnsProdCommerce9Price.ShowPriceListType'}
            );
            response_x = response_map_x.get('response_x');
            return response_x;
        }
    }
}