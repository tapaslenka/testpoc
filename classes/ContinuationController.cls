public with sharing class ContinuationController {
    public AsyncWwwParasoftComWsdlCalculator.addResponse_elementFuture addFuture;
    public String result {get; set;}

    public Continuation startRequest() {    
       Integer TIMEOUT_INT_SECS = 60;  
       Continuation cont = new Continuation(TIMEOUT_INT_SECS);
       cont.continuationMethod = 'processResponse';

       AsyncWwwParasoftComWsdlCalculator.AsyncICalculator  calc = new AsyncWwwParasoftComWsdlCalculator.AsyncICalculator();
       addFuture = calc.beginAdd(cont, 2, 5);      

       return cont;   
    }    

    public Object processResponse() {    
       result = String.valueOf(addFuture.getValue());

       return null; 
    }
}