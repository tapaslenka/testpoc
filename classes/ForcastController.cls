public class ForcastController {
    public List<ForcastObj__c> forecasts{get; set;}
    
    public ForcastController (){
        this.forecasts= [Select Id, Name, Current_value__c,Future_value__c,Indicator__c,Percentage_Change__c From ForcastObj__c LIMIT 10];
    }
}