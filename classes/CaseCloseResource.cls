@RestResource(urlMapping='/case/*/closecase')
global class CaseCloseResource {
	@HttpPost
    global static void closeCases(String caseCloseReason) {
        
        RestRequest req = RestContext.request;
        String[] uriParts = req.requestURI.split('/');
        Id caseId = uriParts[2];
        List<CaseService.CaseCloseInfo> caseCloseInfoList = new List<CaseService.CaseCloseInfo>();
        CaseService.CaseCloseInfo cs = new CaseService.CaseCloseInfo();
        cs.caseCloseReason = caseCloseReason;
        cs.caseId = caseId;
        caseCloseInfoList.add(cs);
        CaseService.closeCases(caseCloseInfoList);     
    }	
}