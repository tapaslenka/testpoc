public class ContinuationControllerExt {
public AsyncWwwIbmComXmlnsProdCommerce9Price.ShowPriceListTypeFuture  addFuture;
    public String result {get; set;}

    public Continuation startRequest() {    
       Integer TIMEOUT_INT_SECS = 60;  
       Continuation cont = new Continuation(TIMEOUT_INT_SECS);
       cont.continuationMethod = 'processResponse';

       AsyncLindeComPiAA2aGlobalEchannelIn015.AsyncHTTPS_Port    calc = new AsyncLindeComPiAA2aGlobalEchannelIn015.AsyncHTTPS_Port  ();
       addFuture = calc.beginGetPriceList(cont);      

       return cont;   
    }    

    public Object processResponse() {    
       result = String.valueOf(addFuture.getValue());

       return null; 
    }
}