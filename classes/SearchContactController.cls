public class SearchContactController {
    
    @AuraEnabled
    public static List < String > getOptionValuesList(sObject objObject, string fld) {
        List < String > allOpts = new list < String > ();
        Schema.sObjectType objType = objObject.getSObjectType();
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap();
        list < Schema.PicklistEntry > values =
            fieldMap.get(fld).getDescribe().getPickListValues();
        for (Schema.PicklistEntry a: values) {
            allOpts.add(a.getValue());
        }
        
        allOpts.sort();
        return allOpts;
    }
    //To be replaced by actual code 
    @AuraEnabled
    public static List<Contact> getContacts() {
        List<Contact> contacts = 
                [SELECT Id, Name, Email, Phone,Contact_Type__c FROM Contact LIMIT 5];

        
        return contacts;
    }
    //To Be replaced by actual code
    @AuraEnabled
    public static List<Contact> searchContactList(String firstName,String lastName,String contactType) {
        String nameStr = firstName+''+Lastname;
        system.debug('------Name -----'+nameStr);
        List<Contact> contactList = 
                [SELECT Id, Name, Email, Phone,Contact_Type__c FROM Contact where name =: nameStr];
        system.debug('------contactList -----'+contactList);
        return contactList;
    }
}